# PumpkinPi


```
 ██▓███   █    ██  ███▄ ▄███▓ ██▓███   ██ ▄█▀ ██▓ ███▄    █  ██▓███   ██▓
▓██░  ██▒ ██  ▓██▒▓██▒▀█▀ ██▒▓██░  ██▒ ██▄█▒ ▓██▒ ██ ▀█   █ ▓██░  ██▒▓██▒
▓██░ ██▓▒▓██  ▒██░▓██    ▓██░▓██░ ██▓▒▓███▄░ ▒██▒▓██  ▀█ ██▒▓██░ ██▓▒▒██▒
▒██▄█▓▒ ▒▓▓█  ░██░▒██    ▒██ ▒██▄█▓▒ ▒▓██ █▄ ░██░▓██▒  ▐▌██▒▒██▄█▓▒ ▒░██░
▒██▒ ░  ░▒▒█████▓ ▒██▒   ░██▒▒██▒ ░  ░▒██▒ █▄░██░▒██░   ▓██░▒██▒ ░  ░░██░
▒▓▒░ ░  ░░▒▓▒ ▒ ▒ ░ ▒░   ░  ░▒▓▒░ ░  ░▒ ▒▒ ▓▒░▓  ░ ▒░   ▒ ▒ ▒▓▒░ ░  ░░▓  
░▒ ░     ░░▒░ ░ ░ ░  ░      ░░▒ ░     ░ ░▒ ▒░ ▒ ░░ ░░   ░ ▒░░▒ ░      ▒ ░
░░        ░░░ ░ ░ ░      ░   ░░       ░ ░░ ░  ▒ ░   ░   ░ ░ ░░        ▒ ░
            ░            ░            ░  ░    ░           ░           ░  
```

PumpkinPi is a simple project to turn a regular Pumpkin in to a Raspberry Pi powered creepy halloween display.

## Features

* Creepy lights
* Haunting background music
* Chilling sound when near

## Videos

https://www.youtube.com/playlist?list=PLejcEoP-xRvTVW2mOFEPEh4PEDO2e1YvL

## Requirements

* Raspberry Pi 1
* PIR sensor
* NeoPixel LED strip
* Breadboard
* USB sound card + Speakers
* Dual port USB power supply

___This is only the initial documentation, and more details will be added once the project is complete___

## Raspberry Pi Setup

### Prepare SD Card

Installing the Debian Lite via macOS

https://www.raspberrypi.org/documentation/installation/installing-images/mac.md

```
sudo dd bs=1m if=path_of_your_image.img of=/dev/rdiskn
```

### Setup Wifi

You will need to plug in a wifi dongle.

This is how to connect from macOS via a direct ethernet connection to setup wifi.

* Enable sharing for the mac wifi interface with the ethernet interface. _Prefernces > Sharing_
* Plug the Raspberry Pi into the ethernet port on the mac. _regular cable will work_
* Plug the Raspberry Pi in to the mac USB for power.
* Wait a bit then run ```arp -a``` and look for the IP address the mac should have given the Pi. 
This is usually __192.168.2.2__
* You should now be able to ```ssh pi@192.168.2.2``` to connect and login
* Now follow this guide https://www.raspberrypi.org/documentation/configuration/wireless/wireless-cli.md
* Once you are connected to the wifi and have an IP address, note it down and turn off the Pi
* Unplug the ethernet cable and turn off the connection sharing on the mac.
* Power up the Pi again. You should only have the USB power connected.
* Once booted you should be able to ssh to the wifi address you noted down. 
__Note this address may change depending on your DHCP lease time__

### Setup Audio

Due to the NeoPixel using PWM it conflicts with the on-board audio.

To get around this problem use a USB sound card which can be purchased online for about £3
 
Follow this handy guide for setup: https://learn.adafruit.com/usb-audio-cards-with-a-raspberry-pi/

### NeoPixel Setup

The following guide is for adding the NeoPixel to the Raspberry Pi.

https://learn.adafruit.com/neopixels-on-raspberry-pi/

We are using the 74AHCT125 level converter for this project

### Running

```
sudo python start.py
```

### Notes

GPIO Zero: https://gpiozero.readthedocs.io/

Handy pinout guide: https://pinout.xyz/

https://www.raspberrypi.org/documentation/linux/software/python.md

sudo apt-get install python-pip
sudo apt-get install python-pygame

ctrl + \ to break script

For multiple networks add another network object along with id_str="ID" properties.

TODO
* Start/Stop button


## Licences

```
Vanishing Kevin MacLeod (incompetech.com)
Licensed under Creative Commons: By Attribution 3.0 License
http://creativecommons.org/licenses/by/3.0/
```
```
motion.wav Licence unknown
```